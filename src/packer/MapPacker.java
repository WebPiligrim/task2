package packer;


import entity.Musician;

import java.util.HashMap;
import java.util.Map;

public class MapPacker {

    private static Map<String, Musician> map = new HashMap<String, Musician>();

    public static Map<String, Musician> getMap() {

        map.put("Alternative", new Musician("Nirvana", "Kurt Kobein,Dave Grol and others"
                , "Vashington"
                , 31
                , 150000));
        map.put("Rap", new Musician("ЛСП", "Oleg Savchenko"
                , "Minsk"
                , 23
                , 15000));
        map.put("Post-Hardcore", new Musician("Asking Alexandria", "Denny Yorsnop,Ben Bruss and others"
                , "York"
                , 52
                , 70000));
        map.put("Metalcore", new Musician("Bring Me The Horizon", "Oliver Syks,Jhordan Fish and others"
                , "Yor  kshir"
                , 44
                , 180000));
        map.put("Rave", new Musician("Crystal Castles", "Elias Glass,Etan Kit o"
                , "Toronto"
                , 36
                , 30000));

        return map;
    }


}
