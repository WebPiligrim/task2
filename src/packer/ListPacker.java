package packer;

import entity.Musician;

import java.util.ArrayList;
import java.util.List;

public class ListPacker {

    private static List<Musician> list = new ArrayList<Musician>();

    public static List<Musician> getList() {
        list.add(new Musician("Nirvana", "Kurt Kobein,Dave Grol and others"
                , "Vashington"
                , 31
                , 150000));
        list.add(new Musician("ЛСП", "Oleg Savchenko"
                , "Minsk"
                , 23
                , 15000));
        list.add(new Musician("Asking Alexandria", "Denny Yorsnop,Ben Bruss and others"
                , "York"
                , 52
                , 70000));
        list.add(new Musician("Bring Me The Horizon", "Oliver Syks,Jhordan Fish and others"
                , "Yorkshir"
                , 44
                , 180000));
        list.add(new Musician("Crystal Castles", "Elias Glass,Etan Kit"
                , "Toronto"
                , 36
                , 30000));

        return list;
    }


}
