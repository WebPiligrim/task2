package sort;

import entity.Musician;

import java.util.*;

public class ByNameComparator implements Comparator<Musician> {
    @Override
    public int compare(Musician musician, Musician t1) {
        return musician.getName().compareTo(t1.getName());
    }


    public static Map<String, Musician> compareTo(Map<String, Musician> disk) {
        List<Map.Entry<String, Musician>> list =
                new LinkedList<Map.Entry<String, Musician>>(disk.entrySet());
        Collections.sort(list, new Comparator<Map.Entry<String, Musician>>() {
            @Override
            public int compare(Map.Entry<String, Musician> stringMusicianEntry, Map.Entry<String, Musician> t1) {
                int resultOfSortByName = stringMusicianEntry.getValue().getName().compareTo(t1.getValue().getName());
                if (resultOfSortByName == 0) {
                    return resultOfSortByName;
                }
                return stringMusicianEntry.getValue().getOrigin().compareTo(t1.getValue().getOrigin());
            }
        });

        Map<String, Musician> sortedMap = new HashMap<>();
        for (Map.Entry<String, Musician> entry : list) {
            sortedMap.put(entry.getKey(), entry.getValue());
        }
        disk = sortedMap;
        return disk;
    }
}
