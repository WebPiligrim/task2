package entity;

public class Musician {

    private String name;

    private String members;

    private String origin;

    private Integer nuberOfSongs;

    private Integer thebestFee;


    public Musician(String name, String members, String origin, Integer nuberOfSongs, Integer thebestFee) {
        this.name = name;
        this.members = members;
        this.origin = origin;
        this.nuberOfSongs = nuberOfSongs;
        this.thebestFee = thebestFee;
    }

    public Integer getThebestFee() {
        return thebestFee;
    }

    public void setThebestFee(int thebestFee) {
        this.thebestFee = thebestFee;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMembers() {
        return members;
    }

    public void setMembers(String members) {
        this.members = members;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public Integer getNuberOfSongs() {
        return nuberOfSongs;
    }

    public void setNuberOfSongs(int nuberOfSongs) {
        this.nuberOfSongs = nuberOfSongs;
    }
}
