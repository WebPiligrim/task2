package util;

import entity.Musician;
import sort.ByNameComparator;
import sort.ByOriginComparator;

import java.util.*;

public class MapMethods implements IContainerManipulation {

    private Map<String, Musician> disk;

    public MapMethods(Map<String, Musician> disk) {
        this.disk = disk;
    }

    @Override
    public void removeMaxElement() {
        int max = 0;
        Musician musicianWithMaxValueOfSongs = disk.get(0);
        for (String key : disk.keySet()) {
            if (max < disk.get(key).getNuberOfSongs()) {
                max = disk.get(key).getNuberOfSongs();
                musicianWithMaxValueOfSongs = disk.get(key);
            }
        }
        disk.remove(musicianWithMaxValueOfSongs);
    }

    @Override
    public void removeMinElement() {
        int min = 0;
        Musician musicianWithMaxValueOfSongs = disk.get(0);
        for (String key : disk.keySet()) {
            if (min > disk.get(key).getNuberOfSongs()) {
                min = disk.get(key).getNuberOfSongs();
                musicianWithMaxValueOfSongs = disk.get(key);
            }
        }
        disk.remove(musicianWithMaxValueOfSongs);

    }

    @Override
    public void removeElementMoreThanX(int x) {

        Map<String, Musician> map = new HashMap<String, Musician>();
        for (String key : disk.keySet()) {
            if (x < disk.get(key).getNuberOfSongs()) {
                map.put(key, disk.get(key));
            }
        }
        for (String key : disk.keySet()) {
            disk.remove(disk.get(key));
        }
    }

    @Override
    public void removeElementLessThanX(int x) {
        Map<String, Musician> map = new HashMap<String, Musician>();
        for (String key : disk.keySet()) {
            if (x > disk.get(key).getNuberOfSongs()) {
                map.put(key, disk.get(key));
            }
        }
        for (String key : disk.keySet()) {
            disk.remove(disk.get(key));
        }
    }

    @Override
    public int calculateTotalSumm() {
        int summ = 0;
        for (String key : disk.keySet()) {
            summ += disk.get(key).getThebestFee();
        }
        System.out.println(summ);
        return summ;
    }

    @Override
    public boolean doesEachElementContainACharter(char j) {
        int flag = 0;
        boolean containACharter = false;
        List<String> names = new ArrayList<String>();
        for (String key : disk.keySet()) {
            names.add(disk.get(key).getName());
        }
        for (String name : names) {
            int indexX = name.indexOf(j);
            if (indexX != -1) {
                flag++;
            }
        }
        if (flag == names.size()) {
            containACharter = true;
        }
        return containACharter;

    }

    @Override
    public void sortByNameAndOrigin() {
        ByNameComparator.compareTo(disk);
        ByOriginComparator.compareTo(disk);

    }

    public Musician returnThirdElement() {
        return disk.get(2);
    }


    public Map<String, Musician> returnSecondAndThirdElement() {
        Map<String, Musician> third = new HashMap<String, Musician>();

        third.put("Second", disk.get("Rap"));
        third.put("Third", disk.get("Post-Hardcore"));
        System.out.println(third);
        return third;
    }


    public Map<String, Musician> returnElementByTemplate() {
        Map<String, Musician> third = new HashMap<String, Musician>();
        String city = "Toronto";

        for (String key : disk.keySet()) {
            if (disk.get(key).getOrigin().equals(city)) {
                third.put(key, disk.get(key));
            }
        }
        return third;
    }

    public void callAllMethods() {

        returnElementByTemplate();
        returnSecondAndThirdElement();
        returnThirdElement();
        doesEachElementContainACharter('i');
        sortByNameAndOrigin();
        calculateTotalSumm();
        removeMaxElement();
        removeMinElement();
        removeElementLessThanX(25);
        removeElementMoreThanX(43);


    }
}
