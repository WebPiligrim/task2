package util;

import entity.Musician;

import java.util.List;
import java.util.stream.Collectors;

public class StreamListMethods implements IContainerManipulation {

    private List<Musician> disk;

    public StreamListMethods(List<Musician> disk) {
        this.disk = disk;
    }


    @Override
    public void removeMaxElement() {
        disk.remove(disk.stream().max((p1, p2) -> p1
                .getNuberOfSongs()
                .compareTo(p2
                        .getNuberOfSongs()))
                .get());
    }

    @Override
    public void removeMinElement() {
        disk.remove(disk.stream().min((p1, p2) -> p1
                .getNuberOfSongs()
                .compareTo(p2
                        .getNuberOfSongs()))
                .get());
    }

    @Override
    public void removeElementMoreThanX(int x) {
        System.out.println(disk);
        System.out.println(disk.stream().filter((p) -> p.getNuberOfSongs() < x).collect(Collectors.toList()));
    }

    @Override
    public void removeElementLessThanX(int x) {
        disk.stream().filter((p) -> p.getNuberOfSongs() > x).collect(Collectors.toList());

    }

    @Override
    public int calculateTotalSumm() {
        return disk.stream().mapToInt((s) -> Integer.parseInt("" + s.getThebestFee())).sum()/2;
    }

    @Override
    public boolean doesEachElementContainACharter(char j) {
        return disk.stream().allMatch((s) -> s.getName().contains("" + j));
    }

    @Override
    public void sortByNameAndOrigin() {
        disk.stream().sorted((o1, o2) -> o1.getName().compareTo(o2.getName()) == 0 ?
                o1.getOrigin().compareTo(o2.getOrigin()) : o1.getName().compareTo(o2.getName()))
                .collect(Collectors.toList());
    }


    public Musician returnThirdElement() {
        return disk.stream().skip(2).findFirst().get();
    }

    public void addCharToName() {
        disk.stream().map((s) -> s.getName() + "_1").collect(Collectors.toList());
    }

    public List<Musician> returnSecondAndThirdElement() {
        System.out.println(disk.stream().skip(1).limit(2).collect(Collectors.toList()));
        return disk.stream().skip(1).limit(2).collect(Collectors.toList());
    }


    public List<Musician> returnElementByTemplate() {
        return disk.stream().filter((s) -> s.getOrigin()
                .contains("Toronto")).collect(Collectors.toList());
    }

    public void callAllMethods() {
        returnThirdElement();
        returnElementByTemplate();
        returnSecondAndThirdElement();
        doesEachElementContainACharter('i');
        sortByNameAndOrigin();
        calculateTotalSumm();
        removeMaxElement();
        removeMinElement();
        removeElementLessThanX(25);
        removeElementMoreThanX(43);
        addCharToName();
    }
}