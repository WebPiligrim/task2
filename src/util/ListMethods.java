package util;

import entity.Musician;
import sort.ByNameComparator;
import sort.ByOriginComparator;

import java.util.ArrayList;
import java.util.List;

public class ListMethods implements IContainerManipulation {

    private List<Musician> disk;


    public ListMethods(List<Musician> disk) {
        this.disk = disk;
    }

    @Override
    public void removeMaxElement() {
        int max = 0;
        Musician musicianWithMaxValueOfSongs = disk.get(0);
        for (Musician musician : disk) {
            if (max < musician.getNuberOfSongs()) {
                max = musician.getNuberOfSongs();
                musicianWithMaxValueOfSongs = musician;
            }
        }
        disk.remove(musicianWithMaxValueOfSongs);
    }

    @Override
    public void removeMinElement() {
        int min = disk.get(0).getNuberOfSongs();
        Musician musicianWithMinValueOfSongs = disk.get(0);
        for (Musician musician : disk) {
            if (min > musician.getNuberOfSongs()) {
                min = musician.getNuberOfSongs();
                disk.iterator().remove();
            }

        }
    }

    @Override
    public void removeElementMoreThanX(int x) {
        List<Musician> mas = new ArrayList<Musician>();
        for (Musician musician : disk) {
            if (x < musician.getNuberOfSongs()) {
                mas.add(musician);
            }
        }
            disk.removeAll(mas);
    }

    @Override
    public void removeElementLessThanX(int x) {
        List<Musician> mas = new ArrayList<Musician>();
        for (Musician musician : disk) {
            if (x > musician.getNuberOfSongs()) {
                mas.add(musician);
            }
        }
        for (Musician musician : mas) {
            disk.remove(musician);
        }
    }

    @Override
    public int calculateTotalSumm() {
        int summTotalFee = 0;
        for (Musician musician : disk) {
            summTotalFee += musician.getThebestFee();
        }
        System.out.println(summTotalFee);
        return summTotalFee / 2;
    }


    public Musician returnThirdElement() {
        System.out.println(disk.get(2));
        return disk.get(2);
    }


    public List<Musician> returnSecondAndThirdElement() {
        List<Musician> third = new ArrayList<Musician>();
        third.add(disk.get(1));
        third.add(disk.get(2));
        System.out.println(third);
        return third;
    }


    public List<Musician> returnElementByTemplate() {
        List<Musician> third = new ArrayList<Musician>();
        String city = "Toronto";
        for (Musician musician : disk) {
            if (musician.getOrigin().equals(city)) {
                third.add(musician);
            }
        }
        return third;
    }

    @Override
    public boolean doesEachElementContainACharter(char x) {
        for (Musician musician : disk) {
            int indexX = musician.getMembers().indexOf("" + x);
            if (indexX == -1) {

                return false;

            }
        }

        return true;
    }

    public void addCharToName() {
        for (Musician musician : disk) {
            String oldName = musician.getName();
            musician.setName("_1" + oldName);
        }
    }

    @Override
    public void sortByNameAndOrigin() {
        disk.sort(new ByNameComparator());
        disk.sort(new ByOriginComparator());
    }

    public void callAllMethods() {
        returnElementByTemplate();
        returnSecondAndThirdElement();
        returnThirdElement();
        doesEachElementContainACharter('i');
        sortByNameAndOrigin();
        calculateTotalSumm();
        removeMaxElement();
        removeMinElement();
        removeElementLessThanX(25);
        removeElementMoreThanX(43);
        addCharToName();
    }

}
